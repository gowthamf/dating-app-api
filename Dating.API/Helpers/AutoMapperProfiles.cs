﻿using AutoMapper;
using Dating.API.DTO;
using Dating.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dating.API.Helpers
{
    public class AutoMapperProfiles:Profile
    {
        public AutoMapperProfiles ( )
        {
            CreateMap<User, UserForListDto> ()
                .ForMember (destinationMember => destinationMember.PhotoUrl, opt => opt.MapFrom (src => src.Photos.FirstOrDefault (p => p.IsMain).Url))
                .ForMember (dest => dest.Age, opt => opt.ResolveUsing (d => d.DateOfBirth.CalculateAge ()));
            CreateMap<User, UserForDetailDTO> ().ForMember (destinationMember => destinationMember.PhotoUrl, opt => opt.MapFrom (src => src.Photos.FirstOrDefault (p => p.IsMain).Url))
                .ForMember (dest => dest.Age, opt => opt.ResolveUsing (d => d.DateOfBirth.CalculateAge ()));
            CreateMap<Photo, PhotosForDetailDto> ();
        }
    }
}
